import React, { useState, useEffect } from 'react'
import MostrarTempo from './MostraTempo'
import MostrarVoltas from './MostraVoltas'
import Button from './Button'
import './styles.css'

 
function App() {
  //hooks State's
  const [numVoltas, setNumVoltas] = useState(1)
  const [running, setRunning] = useState(false)
  const [tempo, setTempo] = useState(0)
  //hook effect
  useEffect(() => {
    let timer = null
    if (running) {
      timer = setInterval(() => {
      setTempo( old => old + 1)
      }, 1000)
    }
    return () => {
      if (timer) {//trufhy valor que é convertido para verdadeiro
        clearInterval(timer)
      }
    }  
  },[running])
  //funções
  const toogleRunning = () => {
    setRunning(!running)
  }
  
  const incremente = () => {
    setNumVoltas(numVoltas + 1)
  }
  const decremente = () => {
    //Não imprimir número negaivo.
    if (numVoltas > 0) {
      setNumVoltas(numVoltas - 1)
    }
  }
  const reset = () => {
    setTempo(0)
    setNumVoltas(0)
  }
  return (
    <div className='App'>
      <MostrarVoltas voltas = {numVoltas} />
      <Button text='+' className='bigger' onClick = {incremente} />
      <Button text='-' className='bigger' onClick = {decremente} />
      {
        numVoltas > 0 &&
        <MostrarTempo tempo= {Math.round(tempo / numVoltas)} />
      }
      <Button text = {running ? 'Pausar' : 'Iniciar'} onClick = {toogleRunning} />
      <Button text = 'Reiniciar' onClick = {reset}/>
    </div>
  );
}

export default App
